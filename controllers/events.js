const models = require('../models');

var getAllEvents = (req, res, next) => {
	// models.Event.findAll({  include: { models: Actor, as: 'actor' } })
	models.Event.findAll({ include: { models: Actor, as: 'actor' }  })
	.then(events =>{
		res.json(events);	
		
	})
};

var addEvent = (req, res, next) => {

	return models.Event.create({
		id: req.body.id,
		type: req.body.type,
		actor_id: req.body.actor.id,
		repo_id: req.body.repo.id
	}).then(events => {
		res.json({
	    "status_code" : 200
	    
	  });
	}).catch(function (err) {
		
		if(err.message == "Validation error"){
			res.json({
			    "status_code" : 400
	  		});
		}else{
			res.json({"message" : err.message});
		}		
		
  		
	});

};


var getByActor = (req, res, next) => {


	

};


var eraseEvents = (req, res, next) => {
		return models.Event.destroy({
		
			where: {},
			truncate: true	

			}).then(events => {
			res.json({
	    		"status_code" : 200
	    
	  		});
		}).catch(function (err) {
				res.json({
			    "Message" : err.message
			    
			  });
		
		
  		
	});
};

module.exports = {
	getAllEvents: getAllEvents,
	addEvent: addEvent,
	getByActor: getByActor,
	eraseEvents: eraseEvents
};

















