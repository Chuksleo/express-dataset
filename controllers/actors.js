const models = require('../models');

var getAllActors = (req, res, next) => {
	models.Actor.findAll()
	.then(actors =>{
		res.json(actors);
	}).catch(function (err) {
		
		res.json(err.message);
  		
	});
		
};



var addActor = (req, res, next) => {
    
	return models.Actor.create({
		id: req.body.id,
		login: req.body.login,
		avatar_url: req.body.avatar_url
	}).then(actors => {
		res.json({
	    "Message" : "Created Actor.",
	    "Item" : actors.id
	  });
	}).catch(function (err) {
		
		if(err.message == "Validation error"){
			res.json({
			    "status_code" : 400
	  		});
		}else{
			res.json(err.message);
		}		
		
  		
	});
		
}; 

var updateActor = (req, res, next) => {

	return models.Actor.update({		
		avatar_url: req.body.avatar_url
	}, { where:{
		id: req.body.id
		}
	}).then(result => {
		if(result == 0){
			res.json({
			    "status_code" : 400
	  		});
		}else{
			res.json({
			    "status_code" : 200
	  		});

		}
		
	}).catch(function (err) {
		res.json({
	    "Message" : err.message
	    
	  });
		
		
  		
	});
};

var getStreak = () => {

};


module.exports = {
	updateActor: updateActor,
	getAllActors: getAllActors,
	addActor:addActor,
	getStreak: getStreak
};

















