var express = require('express');
var router = express.Router();



let events = require("../controllers/events");

router.get("/", events.getAllEvents);
router.post("/add", events.addEvent);



module.exports = router;