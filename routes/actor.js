var express = require('express');
var router = express.Router();

// Routes related to actor.

// Routes related to event
let actors = require("../controllers/actors");

router.get("/", actors.getAllActors);
router.post("/add", actors.addActor);
router.put("/update", actors.updateActor);
module.exports = router;