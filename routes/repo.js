var express = require('express');
var router = express.Router();

// Routes related to actor.

// Routes related to event
let repos = require("../controllers/repos");

router.get("/", repos.getAllRepos);
router.post("/add", repos.addRepo);
module.exports = router;