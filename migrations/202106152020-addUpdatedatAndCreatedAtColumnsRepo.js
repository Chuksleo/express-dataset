'use strict';
module.exports = {
  up(queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'Repos', 
        'createdAt',
        {
          type: Sequelize.DATE,
          allowNull: false,
        },
      ),
      queryInterface.addColumn(
        'Repos',
        'updatedAt',
        {
          type: Sequelize.DATE,
          allowNull: false,
        },
      ),
    
    ]);
  },

  down(queryInterface, Sequelize) {
    // logic for reverting the changes
    return Promise.all([
      queryInterface.removeColumn('Repos', 'createdAt'),
      queryInterface.removeColumn('Repos', 'updatedAt'),
     
    ]);
  },
};
