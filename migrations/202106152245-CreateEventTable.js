'use strict';
module.exports = {

	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Events', { 
			id:{
				allowNull: false,
				primaryKey: true,
				unique: true,
				type: Sequelize.BIGINT,
				
			},
			type:{
				allowNull: false,
				type:Sequelize.STRING,
			},
			actor_id:{
				allowNull: false,
				type: Sequelize.BIGINT,
				references: {
	                model: 'Actors',
	                key: 'id'
            	}
			},
			repo_id:{
				allowNull: false,
				type: Sequelize.BIGINT,
				references: {
	                model: 'Repos',
	                key: 'id'
            	}
			},

			updatedAt:{
				allowNull: false,
				type: Sequelize.DATE,
			},

			createdAt:{
				allowNull: false,
				type: Sequelize.DATE,
			},
			

		});

	},

	down: (queryInterface, Sequelize) => { 
		return queryInterface.dropTable('Events');


	}

}