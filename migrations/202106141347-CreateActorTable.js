'use strict';
module.exports = {

	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Actors', { 
			id:{
				allowNull: false,
				primaryKey: true,
				unique: true,
				type: Sequelize.INTEGER,
				
			},
			login:{
				allowNull: false,
				type:Sequelize.STRING,
			},
			avatar_url:{
				allowNull: false,
				type: Sequelize.STRING,
			},
			

		});

	},

	down: (queryInterface, Sequelize) => { 
		return queryInterface.dropTable('Actors');


	}

}