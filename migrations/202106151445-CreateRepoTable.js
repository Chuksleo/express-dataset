'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Repos', { 
			id:{
				allowNull: false,
				primaryKey: true,
				unique: true,
				type: Sequelize.INTEGER,
			},
			name:{
				allowNull: false,
				type:Sequelize.STRING,
			},
			url:{
				allowNull: false,
				type:Sequelize.STRING
				
			},
			
			

		});

	},

	down: (queryInterface, Sequelize) => { 
		return queryInterface.dropTable('Repos');


	}

}
