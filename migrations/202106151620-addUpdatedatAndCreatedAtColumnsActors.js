'use strict';
module.exports = {
  up(queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'Actors', 
        'createdAt',
        {
          type: Sequelize.DATE,
          allowNull: false,
        },
      ),
      queryInterface.addColumn(
        'Actors',
        'updatedAt',
        {
          type: Sequelize.DATE,
          allowNull: false,
        },
      ),
    
    ]);
  },

  down(queryInterface, Sequelize) {
    // logic for reverting the changes
    return Promise.all([
      queryInterface.removeColumn('Actors', 'createdAt'),
      queryInterface.removeColumn('Actors', 'updatedAt'),
     
    ]);
  },
};
