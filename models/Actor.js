'use strict';	

module.exports = (sequelize, DataTypes) => {
	var Actor = sequelize.define('Actor', { 
		id:{
				 
			type: DataTypes.INTEGER,

			allowNull: false,
			primaryKey: true
		},
		login:{				
			type:DataTypes.STRING,
			allowNull: false
		},
		avatar_url:{				
			type: DataTypes.STRING,
			allowNull: false
		}
			

	});

	Actor.associate = function (models) {
    	Actor.hasMany(models.Event, {foreignKey: 'actor_id'});
	};

return Actor;
}