'use strict';	

module.exports = (sequelize, DataTypes) => {
	var Event = sequelize.define('Event', { 
		id:{
				 
			type: DataTypes.BIGINT,

			allowNull: false,
			primaryKey: true
		},
		type:{				
			type:DataTypes.STRING,
			allowNull: false
		},
		actor_id:{				
			type: DataTypes.BIGINT,
			allowNull: false
		},
		repo_id:{				
			type: DataTypes.BIGINT,
			allowNull: false
		}
			

	});


	Event.associate = function (models) {
    Event.belongsTo(models.Actor, {foreignKey: 'actor_id', target_key: 'id'});
    
};

	// Event.associate = function(models) {
 //    Event.belongsTo(models.Actor);
 //  };
 	// Event.belongsTo(Actor);

	// Event.associate = function (models) {
	//     Event.login = Event.belongsTo(models.Actor, {foreignKey: 'id', target_key: 'actor_id'});
	    
	// };
	// Event.associate = function (models) {
	//     Event.hasMany(models.Actor, {foreignKey: 'actor_id'});
	// };
return Event;
}