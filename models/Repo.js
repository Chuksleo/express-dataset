'use strict';	

module.exports = (sequelize, DataTypes) => {
	var Repo = sequelize.define('Repo', { 
		id:{
				 
			type: DataTypes.INTEGER,

			allowNull: false,
			primaryKey: true
		},
		name:{				
			type:DataTypes.STRING,
			allowNull: false
		},
		url:{				
			type: DataTypes.STRING,
			allowNull: false
		}
			

	});

return Repo;
}