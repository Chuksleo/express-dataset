module.exports = {
  "development": {
    "username": "express-dataset-user",
    "password": "12345",
    "database": "express_db",
    "host": "127.0.0.1",
    "dialect": "postgres",
    "port"  : 5432
  },
  "test": {
    "username": "postgres",
    "password": null,
    "database": "datasetdb",
    "host": "127.0.0.1",
    "dialect": "postgres"
  },
  "production": {
    "username": "postgres",
    "password": null,
    "database": "datasetdb",
    "host": "127.0.0.1",
    "dialect": "postgres"
  }
}
